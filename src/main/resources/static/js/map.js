let map;
let marker;
const CATEGORY = "map";
const NAME = window.location.search.split("=")[1];
function initMap() {
    document.title = NAME;
    map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: 43.5617391, lng: 1.4658285 },
        zoom: 11,
        draggableCursor : 'default',
        disableDefaultUI : true
    });
    map.addListener('click', function(e) {
        if (marker) {
            removeMarker(marker);
        }
        marker = placeMarker(e.latLng, map);

        let position = {
            lat:e.latLng.lat(),
            lng:e.latLng.lng()
        }
        sendMarker(position);
    });


    function placeMarker(position, map) {
        let marker = new google.maps.Marker({
            position: position,
            map: map
        });
        return marker;
    }
    function removeMarker(marker) {
        marker.setMap(null);
        marker = null;
    }

    function sendMarker(position) {
        position.name = NAME;
        fetch("http://localhost:8080/map", {
            method: "POST",
            body: JSON.stringify(position)
        }).then(res => {
            console.log("position updated")
        });
    }
}

function connect() {
    let socket = new SockJS('/websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, frame => {
        stompClient.subscribe("/existingComponents", response => {
            isStillAlive(response.body);
        });
    });
}

function sendValues() {
    stompClient.send("/app/hello", {} , JSON.stringify({
        "category" : CATEGORY,
        "name" : NAME
    }));
}

function isStillAlive(response) {
    json = JSON.parse(response);
    if (!containsComponent(JSON.parse(response)[CATEGORY])) {
        window.close();
    }
}

function containsComponent(list) {
    let isPresent = false;
    list.forEach(component => {
        if (component.key === NAME) {
            isPresent = true;
        }
    });
    return isPresent;
}

connect();