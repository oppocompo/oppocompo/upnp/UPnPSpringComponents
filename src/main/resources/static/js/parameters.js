let components;
let annotations;
const mainDiv = document.getElementById("container");
const modal = new bootstrap.Modal(document.getElementById('exampleModal'));
const input = document.getElementById("name");
const validerButton = document.getElementById("valider");

let componentToRename = {
    componentCategory:"",
    oldName:"",
    newName:""
};

let noComponentsDeployed = true;

let componentItem;
componentToRename.componentCategory = "";
let oldName = "";

async function loadAnnotations() {
    return new Promise((resolve, reject) => {
        fetch("http://localhost:8080/parametrableComponents", {
            method: "GET"
        }).then(res => {
            return res.json();
        }).then(function (data) {
            resolve(data);
        }).catch(function (err) {
            reject(err);
        });
      });
}

async function getComponents() {
    return new Promise((resolve, reject) => {
        fetch("http://localhost:8080/components", {
            method: "GET"
        }).then(res => {
            return res.json();
        }).then(function (data) {
            resolve(data);
        }).catch(function (err) {
            reject(err);
        });
      });
}

async function loadComponents() {
    components = await getComponents();
    annotations = await loadAnnotations();
    for (let k in components) {
        if (components[k].length >0) {
            noComponentsDeployed = false;
            createOneBloc(k);
        }
    } 

    if (noComponentsDeployed){
        let h2 = document.createElement("h2");
        h2.className = "text-center";
        h2.appendChild(document.createTextNode("There is no components currently deployed"));
        mainDiv.appendChild(h2);
    }

}

function createOneBloc(componentName) {
    let div = document.createElement("div");
    div.id = componentName;
    div.classname = "container-sm";

    let title = document.createElement("h2");
    title.className = "col text-center";
    title.appendChild(document.createTextNode(componentName));

    div.appendChild(title);
    addComponents(componentName,div);

    mainDiv.appendChild(div);

}

function addComponents(componentName, div) {
    let divButtons = document.createElement("div");
    divButtons.className = "d-flex flex-wrap";

    components[componentName].forEach(component => {
        delete Object.assign(component, {["name"]: component["key"] })["key"];
        delete Object.assign(component, {["isOn"]: component["value"] })["value"];
        component.componentCategory = componentName;
        let button = document.createElement("button");
        button.id = component.name;
        button.innerHTML = component.name;
        if (component.isOn) {
            button.className = "btn btn-primary mx-auto";
        } else {
            button.className = "btn btn-danger mx-auto";
        }
        addListener(button,component);
        divButtons.appendChild(button);
    });
    div.appendChild(divButtons);
    div.appendChild(document.createElement("br"));
}

function addListener(button, component) {
    button.onclick = () => {
        let method = "PUT";
        if (component.isOn) {
            method = "DELETE";
        }
        fetch("http://localhost:8080/components", {
        method: method,
        body: JSON.stringify(component)
    }).then(res => {
        component.isOn = !component.isOn;
        if (component.isOn) {
            button.className = "btn btn-primary mx-auto";
        } else {
            button.className = "btn btn-danger mx-auto";
        }
    });
    }
    button.addEventListener("contextmenu", (ev) => {
        ev.preventDefault();
        componentToRename.componentCategory = component.componentCategory;
        componentToRename.oldName = component.name;
        input.value = component.name;
        modal.show();
    });
}

function removeNonAlphaNumericCharacters(str) {
    let newStr = str;
    newStr.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    return newStr.replace(/[^A-Za-z0-9]/g, '');
}

loadComponents();

validerButton.onclick = () => {
    componentToRename.newName = removeNonAlphaNumericCharacters(input.value);
    fetch("http://localhost:8080/components", {
        method: "POST",
        body: JSON.stringify(componentToRename)
    }).then(res => {
        let hasOldName = (component) => component.name = componentToRename.oldName;
        let i = components[componentToRename.componentCategory].findIndex(hasOldName);
        components[componentToRename.componentCategory][i].name = componentToRename.newName;
        let button = document.getElementById(componentToRename.oldName);
        button.id = componentToRename.newName;
        button.innerHTML = componentToRename.newName;
        addListener(button,components[componentToRename.componentCategory][i]);
        if (annotations[componentToRename.componentCategory].hasIHM) {
            window.open(`http://localhost:8080${annotations[componentToRename.componentCategory].url}?name=${componentToRename.newName}`);
        }
        modal.hide();
    });
}