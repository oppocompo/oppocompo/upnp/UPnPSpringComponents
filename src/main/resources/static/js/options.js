let properties;
let componentNames = [];
let components;

async function loadProperties() {
    return new Promise((resolve, reject) => {
        fetch("http://localhost:8080/properties", {
            method: "GET"
        }).then(res => {
            return res.json();
        }).then(function (data) {
            resolve(data);
        }).catch(function (err) {
            reject(err);
        });
      });
}

async function loadAnnotations() {
    return new Promise((resolve, reject) => {
        fetch("http://localhost:8080/parametrableComponents", {
            method: "GET"
        }).then(res => {
            return res.json();
        }).then(function (data) {
            resolve(data);
        }).catch(function (err) {
            reject(err);
        });
      });
}

async function loadValues() {
    properties = await loadProperties();
    for (let k in properties) componentNames.push(k);
    await generateHTML();

    setTogglersValues();

    let buttonValider = document.getElementById("valider");
    buttonValider.onclick = () => { 
        getNewValues();

        fetch("http://localhost:8080/properties", {
            method: "POST",
            body: JSON.stringify(properties)
        }).then(() => {
            for (let k in properties) {
                if (properties[k] >0) {
                    if (components[k].hasIHM) {
                        for (let i=0 ; i<properties[k] ; i++) {
                            window.open(`http://localhost:8080${components[k].url}?name=${k}${i}`);
                        }
                    }
                }
            }
        });
    };
}

function setTogglersValues() {
    componentNames.forEach(component => {
        document.getElementById(component).value = properties[component];
    });
}

function getNewValues() {
    componentNames.forEach(component => {
       properties[component] = document.getElementById(component).value.toString();
    });
}

async function generateHTML() {
    let divContainer = document.createElement("div");
    divContainer.className = "container-sm";
    document.body.appendChild(divContainer);
    components = await loadAnnotations();
    componentNames.forEach(componentName => {
        let component = components[componentName];
        generateOneComponent(componentName, component, divContainer)
    });

    generateValiderButton(divContainer);

}

function generateValiderButton(divContainer) {
    let divButton = document.createElement("div");
    divButton.className = "col text-center";
    let button = document.createElement("button");
    button.innerHTML = "Accept";
    button.id = "valider";
    button.className = "btn btn-primary";
    divButton.appendChild(button);
    divContainer.append(divButton);
}

function generateOneComponent(componentName, component, divContainer) {
    let divComponent = document.createElement("div");
    divComponent.className = "container-sm";

    let title = document.createElement("h2");
    title.className = "col text-center";
    title.appendChild(document.createTextNode(componentName));

    let divInput = document.createElement("div");
    divInput.className = "container-sm";
    let input = document.createElement("input");
    input.id = componentName;
    input.className = "form-control text-center";
    input.type = "number";
    input.min = 0;
    divInput.appendChild(input);

    let divDescription = document.createElement("div");
    divInput.className = "container-sm";

    let divImg = document.createElement("div");
    divImg.className = "d-flex justify-content-center";
    let img = document.createElement("img");
    img.src = "/image?component="+componentName;
    img.className = "img-fluid";
    let p = document.createElement("p");
    p.className = "col text-center";
    p.appendChild(document.createTextNode(component.description));
    
    divImg.appendChild(img);
    divInput.appendChild(divImg);
    divInput.appendChild(p);

    divComponent.appendChild(title);
    divComponent.appendChild(divInput);
    divComponent.appendChild(divDescription);
    divContainer.appendChild(divComponent);
}

loadValues();