const NAME = window.location.search.split("=")[1];
const CATEGORY = "display";
let stompClient = null;

async function getInfos() {
    return new Promise((resolve, reject) => {
        fetch(`http://localhost:8080/infos?name=${NAME}`, {
            method: "GET"
        }).then(res => {
            return res.json();
        }).then(function (data) {
            resolve(data);
        }).catch(function (err) {
            reject(err);
        });
      });
}

async function updateInfos() {
    let infos;
    try {
        infos = await getInfos();
    } catch (e) {
        window.close();
    }
    updateDisplay(infos);

    await new Promise(resolve => setTimeout(resolve, 1000));
    updateInfos();
}

function updateDisplay(infos) {
    let div = document.getElementById("infos");
    div.innerHTML = "";
    for (let key in infos) {
        let title = document.createElement("h2");
        title.className = "col text-center";
        title.appendChild(document.createTextNode(key));
        let description = document.createElement("div");
        description.className = "col text-center";
        description.appendChild(document.createTextNode(infos[key]));

        div.appendChild(title);
        div.appendChild(description);
    }
}

function connect() {
    let socket = new SockJS('/websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, frame => {
        stompClient.subscribe("/existingComponents", response => {
            isStillAlive(response.body);
        });
    });
}

function sendValues() {
    stompClient.send("/app/hello", {} , JSON.stringify({
        "category" : CATEGORY,
        "name" : NAME
    }));
}

function isStillAlive(response) {
    json = JSON.parse(response);
    if (!containsComponent(JSON.parse(response)[CATEGORY])) {
        window.close();
    }
}

function containsComponent(list) {
    let isPresent = false;
    list.forEach(component => {
        if (component.key === NAME) {
            isPresent = true;
        }
    });
    return isPresent;
}

document.title = NAME;
updateInfos();
connect();