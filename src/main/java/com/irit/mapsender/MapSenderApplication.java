package com.irit.mapsender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MapSenderApplication {

    public static void main(String[] args) {
        SpringApplication.run(MapSenderApplication.class, args);
    }

}
