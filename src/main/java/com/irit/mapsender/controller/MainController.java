package com.irit.mapsender.controller;

import com.irit.mapsender.model.beans.Position;
import com.irit.mapsender.model.components.ComponentsManager;
import com.irit.mapsender.model.exceptions.ErrorWhileCreatingComponentException;
import com.irit.mapsender.model.exceptions.MissingConstructorForParametrableComponentException;
import com.irit.mapsender.model.exceptions.NoParametrableComponentFoundException;
import com.irit.mapsender.model.exceptions.NotADisplayableComponentException;
import com.irit.mapsender.model.tools.ComponentDiagramDrawer;
import com.irit.mapsender.model.tools.Pair;
import com.irit.mapsender.model.tools.ParametrableComponentsScanner;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class MainController {

    @Autowired
    protected SimpMessagingTemplate template;

    private ComponentsManager componentsManager;

    private ParametrableComponentsScanner scanner;

    public MainController(){
        componentsManager = new ComponentsManager();
        scanner = new ParametrableComponentsScanner();
    }

    @GetMapping("/")
    public String index(){
        return "index";
    }

    @GetMapping("ihmMenu")
    public String ihmMenu() { return "ihmMenu"; }

    @GetMapping("/displayMenu")
    public String displayMenu() { return "displayMenu";}

    @GetMapping("/options")
    public String options(){
        return "options";
    }

    @GetMapping("/parameters")
    public String parameters() { return "parameters"; }

    @GetMapping("/properties")
    @ResponseBody
    public Map<String,String> getProperties(){
        return componentsManager.getComponentsValue();
    }

    @PostMapping("/properties")
    @ResponseBody
    public void setProperty(@RequestBody String body) {
        JSONObject json = new JSONObject(body);
        for (String key : json.keySet()){
            componentsManager.updateComponent(key, json.getString(key));
        }
        componentsManager.addOrRemoveComponents();
        this.template.convertAndSend("/existingComponents",componentsManager.getAllComponents());
    }

    @GetMapping("/map")
    public String map() { return "map"; }

    @GetMapping("/display")
    public String display() { return "display"; }

    @GetMapping("/mapMenu")
    public String mapMenu() { return "mapMenu"; }

    @GetMapping("/parametrableComponents")
    @ResponseBody
    public Map<String, Object> sendParametrableComponents() {
        return new JSONObject(scanner.getAllAnnotations()).toMap();
    }

    @GetMapping("/infos")
    @ResponseBody
    public Map<String,Object> sendInfos(@RequestParam String name) {
        try {
            JSONObject json = new JSONObject(componentsManager.getInfosToDisplay("display",name));
            return json.toMap();
        } catch (NotADisplayableComponentException e) {
            return null;
        }
    }

    @PostMapping("/map")
    @ResponseBody
    public void updateMap(@RequestBody String body) {
        JSONObject json = new JSONObject(body);
        Position pos = new Position(json.getDouble("lat"),json.getDouble("lng"), null);
        componentsManager.updatePosition(json.getString("name"), pos);
    }

    @GetMapping("/activeMaps")
    @ResponseBody
    public List<String> activeMaps() {
        return componentsManager.getAllMaps();
    }

    @GetMapping("/activeDisplays")
    @ResponseBody
    public List<String> activeDisplays() { return componentsManager.getAllDisplays();}

    @GetMapping("/components")
    @ResponseBody
    public Map<String,List<Pair<String,Boolean>>> getAllComponents() {
        return componentsManager.getAllComponents();
    }

    @PostMapping("/components")
    @ResponseBody
    public void renameComponent(@RequestBody String body) {
        JSONObject json = new JSONObject(body);
        try {
            componentsManager.editComponentName(json.getString("componentCategory"),json.getString("oldName"), json.getString("newName"));
            this.template.convertAndSend("/existingComponents",this.getAllComponents());
        } catch (MissingConstructorForParametrableComponentException e) {
            e.printStackTrace();
        } catch (ErrorWhileCreatingComponentException e) {
            e.printStackTrace();
        } catch (NoParametrableComponentFoundException e) {
            e.printStackTrace();
        }
    }

    @PutMapping("/components")
    @ResponseBody
    public void turnOnComponent(@RequestBody String body) {
        JSONObject json = new JSONObject(body);
        componentsManager.turnOnAComponent(json.getString("componentCategory"), json.getString("name"));
    }

    @DeleteMapping("/components")
    @ResponseBody
    public void turnOffComponent(@RequestBody String body) {
        JSONObject json = new JSONObject(body);
        componentsManager.turnOffAComponent(json.getString("componentCategory"), json.getString("name"));
    }

    @GetMapping(value = "/image",
    produces = "image/svg+xml")
    @ResponseBody
    public byte[] sendSVG(@RequestParam String component) throws IOException {
        return FileUtils.readFileToByteArray(new File(ComponentDiagramDrawer.FILE_PATH+component+ComponentDiagramDrawer.EXTENSION));
    }

    @GetMapping("/exists")
    @ResponseBody
    public Map<String,Boolean> componentExists(@RequestParam String componentCategory, @RequestParam String name) {
        Map<String,Boolean> response = new HashMap<>();
        response.put("exists",componentsManager.componentExists(componentCategory,name));
        return response;
    }
}