package com.irit.mapsender.model.components;

import com.irit.mapsender.model.beans.Position;
import com.irit.mapsender.model.components.cartes.MapComponent;
import com.irit.mapsender.model.exceptions.ErrorWhileCreatingComponentException;
import com.irit.mapsender.model.exceptions.MissingConstructorForParametrableComponentException;
import com.irit.mapsender.model.exceptions.NoParametrableComponentFoundException;
import com.irit.mapsender.model.exceptions.NotADisplayableComponentException;
import com.irit.mapsender.model.tools.Pair;
import com.irit.mapsender.model.tools.ParametrableComponentsScanner;
import com.irit.mapsender.model.tools.PropertiesReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.beans.Introspector;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class ComponentsManager {

    /**
     * première clef : le type de composant
     * seconde clef : le nom du composant
     */
    private Map<String, Map<String,Component>> componentsMap;
    private PropertiesReader propertiesReader;
    private ParametrableComponentsScanner scanner;

    @Autowired
    protected SimpMessagingTemplate template;

    public ComponentsManager() {
        propertiesReader = new PropertiesReader();
        componentsMap = new HashMap<>();
        scanner = new ParametrableComponentsScanner();
        loadComponents();
    }

    private void loadComponents() {

        for (Class clazz : scanner.getParametrableComponents()){

            String simpleName = Introspector.decapitalize(clazz.getSimpleName().split("Component")[0]);
            String fullName = clazz.getCanonicalName();

            int numberOfComponents = Integer.valueOf(propertiesReader.getProperty(simpleName));
            Map<String,Component> componentMap = new HashMap<>();
            for (int i = 0 ; i < numberOfComponents ; i++) {
                Component component = null;
                try {
                    component = createComponentFromName(fullName, simpleName+i);
                    component.addLocalDevice();
                } catch (NoParametrableComponentFoundException | MissingConstructorForParametrableComponentException | ErrorWhileCreatingComponentException exception) {
                    exception.printStackTrace();
                }
                componentMap.put(simpleName+i,component);
            }
            componentsMap.put(simpleName,componentMap);
        }
    }

    private Component createComponentFromName(String className, String name) throws NoParametrableComponentFoundException, MissingConstructorForParametrableComponentException, ErrorWhileCreatingComponentException {
        Component component;
        try {
            Class<?> clazz = Class.forName(className);
            Constructor<?> ctor = clazz.getConstructor(String.class);
            component = (Component) ctor.newInstance(new Object[] { name });
        } catch (ClassNotFoundException e) {
            throw new NoParametrableComponentFoundException(className);
        } catch (NoSuchMethodException e) {
            throw new MissingConstructorForParametrableComponentException(className);
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new ErrorWhileCreatingComponentException(className);
        }
        return component;
    }

    public void addOrRemoveComponents() {
        shutdownAllComponents();
        loadComponents();
    }

    public void updateComponent(String componentName, String value) {
        propertiesReader.editProperty(componentName,value);
    }

    private void shutdownAllComponents(){
        Set<String> keys = componentsMap.keySet();
        for (String keyForCategory : keys) {
            for (String keyForName : componentsMap.get(keyForCategory).keySet()) {
                componentsMap.get(keyForCategory).get(keyForName).removeLocalDevice();
            }
        }
    }

    public void editComponentName(String componentCategory, String oldName, String newName) throws MissingConstructorForParametrableComponentException, ErrorWhileCreatingComponentException, NoParametrableComponentFoundException {
        turnOffAComponent(componentCategory,oldName);
        componentsMap.get(componentCategory).remove(oldName);
        Map<String,String> map = scanner.getComponentsNameMap();
        if (!componentsMap.containsKey(componentCategory)) {
            System.out.println("Catégorie incorrecte, abandon");
            return;
        }
        Component component = createComponentFromName(map.get(componentCategory),newName);
        component.addLocalDevice();
        componentsMap.get(componentCategory).put(newName,component);
    }

    public Boolean componentExists(String componentCategory, String name) {
        System.out.println(componentCategory);
        System.out.println(name);
        System.out.println(componentsMap.keySet());
        System.out.println(componentsMap.get(componentCategory).keySet());
        return componentsMap.get(componentCategory).containsKey(name);
    }

    public Map<String,String> getComponentsValue(){
        Map<String,String> map = new HashMap<>();
        for (Pair<String,String> pair : propertiesReader.getAllProperties()) {
            map.put(pair.getKey(),pair.getValue());
        }
        return map;
    }

    public Map<String,List<Pair<String,Boolean>>> getAllComponents() {
        HashMap<String,List<Pair<String,Boolean>>> map = new HashMap<>();
        for (String componentType: componentsMap.keySet()) {
            List<Pair<String,Boolean>> list = new ArrayList<>();
            map.put(componentType,list);
            for (String componentName : componentsMap.get(componentType).keySet()) {
                list.add(new Pair<>(componentName,componentsMap.get(componentType).get(componentName).isOn()));
            }
        }
        return map;
    }

    public void turnOnAComponent(String componentCategory, String name) {
        componentsMap.get(componentCategory).get(name).addLocalDevice();
    }

    public void turnOffAComponent(String componentCategory, String name) {
        componentsMap.get(componentCategory).get(name).removeLocalDevice();
    }

    public void updatePosition(String name, Position pos) {
        MapComponent map = (MapComponent) componentsMap.get("map").get(name);
        map.callSetLocation(pos);
    }

    public String getInfosToDisplay(String componentCategory, String name) throws NotADisplayableComponentException {
        try{
            DisplayableComponent component = (DisplayableComponent) componentsMap.get(componentCategory).get(name);
            return component.displayInfos();
        }catch (Exception e) {
            throw new NotADisplayableComponentException(name);
        }
    }

    public List<String> getAllMaps() {
        ArrayList<String> names = new ArrayList<>();
        for (String name : componentsMap.get("map").keySet()) {
            if (componentsMap.get("map").get(name).isOn()) {
                names.add(name);
            }
        }
        return names;
    }

    public List<String> getAllDisplays() {
        ArrayList<String> names = new ArrayList<>();
        for (String name : componentsMap.get("display").keySet()) {
            if (componentsMap.get("display").get(name).isOn()) {
                names.add(name);
            }
        }
        return names;
    }
}
