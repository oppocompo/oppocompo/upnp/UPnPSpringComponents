package com.irit.mapsender.model.components.nameGetters.country;

import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;

@ParametrableComponent( requires = {},
        provides = {"Name", "SetLocation"},
        description = "A component that return the name of a country from a location"
)
public class CountryNameGetterComponent extends Component {

    private final int VERSION = 1;
    private LocalService<CountrySetLocationService> localServiceSetLocation;
    private LocalService<CountyNameService> localServiceName;

    public CountryNameGetterComponent(String name) {
        localServiceSetLocation = ServiceFactory.makeLocalServiceFrom(CountrySetLocationService.class);
        localServiceName = ServiceFactory.makeLocalServiceFrom(CountyNameService.class);
        localServiceSetLocation.getManager().getImplementation().setNameService(localServiceName.getManager().getImplementation());

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component that return the name of a country from a location",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceName,localServiceSetLocation
                });
    }
}
