package com.irit.mapsender.model.components.displayPointsAdapter.adapters;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

public class SetLocationSecondAdapterComponent extends Component {
    private static final int VERSION = 1;
    private LocalService<SetLocationSecondAdapterService> localServiceAdapter;
    private LocalService<DependencyInjectionService> localServiceRequires;

    public SetLocationSecondAdapterComponent() {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("SetLocationSecond", new UDAServiceId("SetLocationSecond"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);

        localServiceAdapter = ServiceFactory.makeLocalServiceFrom(SetLocationSecondAdapterService.class);
        localServiceAdapter.getManager().getImplementation().setLocalService(localServiceRequires);

        localDevice = DeviceFactory.makeLocalDevice(
                "SetLocationSecondAdapter",
                "A SetLocation to SetLocationSecond adapter",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceAdapter,localServiceRequires
                });
    }
}
