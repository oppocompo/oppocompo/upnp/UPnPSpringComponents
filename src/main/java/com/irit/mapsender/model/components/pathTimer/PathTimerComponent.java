package com.irit.mapsender.model.components.pathTimer;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

@ParametrableComponent(
        requires = {"BatteryInfo","DisplayInfos"},
        provides = {"DisplayPath"},
        description = "A component that send estimated time and estimated remaining battery time"
)
public class PathTimerComponent extends Component {

    private final int VERSION = 1;
    private LocalService<DisplayPathService> localServiceProvides;
    private LocalService<DependencyInjectionService> localServiceRequires;

    public PathTimerComponent(String name) {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("BatteryInfo", new UDAServiceId("BatteryInfo"));
        map.put("DisplayInfos", new UDAServiceId("DisplayInfos"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);
        localServiceProvides = ServiceFactory.makeLocalServiceFrom(DisplayPathService.class);
        localServiceProvides.getManager().getImplementation().setLocalService(localServiceRequires);

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component that send estimated time and estimated remaining battery time",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceProvides,localServiceRequires
                });
    }
}
