package com.irit.mapsender.model.components.zoomLevel;

import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;

@ParametrableComponent(
        provides = {"ZoomLevel"},
        description = "A component that send a zoom level corresponding to a city")
public class TownZoomLevelComponent extends Component {

    private static final int VERSION = 1;
    private static LocalService<TownZoomLevelService> localService = null;

    public TownZoomLevelComponent(String name) {
        localService = ServiceFactory.makeLocalServiceFrom(TownZoomLevelService.class);

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component that send a zoom level corresponding to a city",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localService
                });
    }
}
