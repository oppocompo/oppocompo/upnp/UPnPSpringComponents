package com.irit.mapsender.model.components.displayPointsGrouper.adapters;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;

import java.util.HashMap;
import java.util.Map;

@UpnpService(
        serviceId = @UpnpServiceId("DisplayPoints"),
        serviceType = @UpnpServiceType("DisplayPoints")
)
public class DisplayPointsSecondAdapterService extends Service {

    @UpnpStateVariable
    private String points;

    @UpnpAction(name="DisplayPoints")
    public void displayPoints(@UpnpInputArgument(name = "Points") String points) {
        this.points = points;

        Map<String, Object> args = new HashMap<>();
        args.put("Points", this.points);

        executeRequire("DisplayPointsSecond","DisplayPointsSecond",args);
    }
}
