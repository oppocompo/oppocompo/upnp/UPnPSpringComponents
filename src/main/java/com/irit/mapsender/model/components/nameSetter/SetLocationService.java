package com.irit.mapsender.model.components.nameSetter;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@UpnpService(
        serviceId = @UpnpServiceId("SetLocation"),
        serviceType = @UpnpServiceType("SetLocation")
)
public class SetLocationService extends Service {

    @UpnpStateVariable
    private String longitude;
    @UpnpStateVariable
    private String latitude;
    @UpnpStateVariable
    private String settings;

    @UpnpAction(name="SetLocation")
    public void setLocation(@UpnpInputArgument(name = "Longitude") String longitude,
                            @UpnpInputArgument(name = "Latitude") String latitude,
                            @UpnpInputArgument(name = "Settings") String settings){
        Map<String, Object> args = new HashMap<>();
        args.put("Longitude",longitude);
        args.put("Latitude",latitude);

        executeRequireAndThen("Name","Name",new HashMap(),actionInvocation -> {
            String name = actionInvocation.getOutput("Name").toString();
            JSONObject json = new JSONObject(settings);
            json.put("name",name);
            args.put("Settings",json.toString());
            executeRequire("SetLocation","SetLocation",args);
        });
    }
}
