package com.irit.mapsender.model.components.nameSetter;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

@ParametrableComponent(
        requires = {"SetLocation","Name"},
        provides = {"SetLocation"},
        description = "A component used to add a name to a position"
)
public class NameSetterComponent extends Component {

    private final int VERSION = 1;
    private LocalService<SetLocationService> localServiceSetLocation;
    private LocalService<DependencyInjectionService> localServiceRequires;

    public NameSetterComponent(String name) {
        localServiceSetLocation = ServiceFactory.makeLocalServiceFrom(SetLocationService.class);
        Map<String, ServiceId> map = new HashMap<>();
        map.put("SetLocation",new UDAServiceId("SetLocation"));
        map.put("Name",new UDAServiceId("Name"));
        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);
        localServiceSetLocation.getManager().getImplementation().setLocalService(localServiceRequires);

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component used to add a name to a position",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceRequires,localServiceSetLocation
                });
    }
}
