package com.irit.mapsender.model.components.battery;

import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;

@ParametrableComponent(
        provides = {"BatteryInfo"},
        description = "A component that send the current battery level")
public class BatteryComponent extends Component {

    private static final int VERSION = 1;
    private LocalService<BatteryInfoService> localService;

    public BatteryComponent(String name) {
        localService = ServiceFactory.makeLocalServiceFrom(BatteryInfoService.class);

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component that send the current battery level",
                VERSION,
                "IRIT",
                new LocalService[]{localService});
    }
}
