package com.irit.mapsender.model.components.zoomLevel;

import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

@ParametrableComponent(
        provides = {"ZoomLevel"},
        description = "A component that send a zoom level corresponding to a country"
)
public class CountryZoomLevelComponent extends Component {

    private static final int VERSION = 1;
    private static LocalService<CountryZoomLevelService> localService = null;

    public CountryZoomLevelComponent(String name) {
        localService = ServiceFactory.makeLocalServiceFrom(CountryZoomLevelService.class);

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component that send a zoom level corresponding to a country",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localService
                });
    }
}
