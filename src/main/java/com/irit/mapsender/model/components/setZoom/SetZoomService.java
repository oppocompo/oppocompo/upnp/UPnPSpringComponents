package com.irit.mapsender.model.components.setZoom;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;

import java.util.HashMap;
import java.util.Map;

@UpnpService(
        serviceId = @UpnpServiceId("SetLocation"),
        serviceType = @UpnpServiceType("SetLocation")
)
public class SetZoomService extends Service {
    @UpnpStateVariable
    private String longitude;
    @UpnpStateVariable
    private String latitude;
    @UpnpStateVariable
    private String settings;

    @UpnpAction(name="SetLocation")
    public void setLocation(@UpnpInputArgument(name = "Longitude") String longitude,
                        @UpnpInputArgument(name = "Latitude") String latitude,
                        @UpnpInputArgument(name = "Settings") String settings) {
        Map<String, Object> args = new HashMap<>();
        args.put("Longitude",longitude);
        args.put("Latitude",latitude);

        executeRequireAndThen("ZoomLevel", "ZoomLevel", new HashMap(), actionInvocation -> {
            String zoom = actionInvocation.getOutput("Zoom").toString();
            args.put("Zoom",zoom);
            executeRequire("SetZoom","SetZoom",args);
        });
    }
}
