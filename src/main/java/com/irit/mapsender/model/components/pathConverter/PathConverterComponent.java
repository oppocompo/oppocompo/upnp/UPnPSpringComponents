package com.irit.mapsender.model.components.pathConverter;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.beans.Path;
import com.irit.mapsender.model.components.Component;
import com.irit.mapsender.model.components.pathConverter.adapters.EndAdapterComponent;
import com.irit.mapsender.model.components.pathConverter.adapters.StartAdapterComponent;
import com.irit.stores.UpnpServiceStore;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

@ParametrableComponent(
        requires = {"DisplayPath"},
        provides = {"SetLocation", "SetLocation"},
        description = "A component used to transform two points into a Path"
)
public class PathConverterComponent extends Component {

    private static final int VERSION = 1;
    private LocalService<SetLocationStartService> localServiceStart;
    private LocalService<SetLocationEndService> localServiceEnd;
    private LocalService<DependencyInjectionService> localServiceRequires;

    private EndAdapterComponent endAdapterComponent;
    private StartAdapterComponent startAdapterComponent;

    public PathConverterComponent(String name) {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("DisplayPath", new UDAServiceId("DisplayPath"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);

        Path path = new Path();

        localServiceStart = ServiceFactory.makeLocalServiceFrom(SetLocationStartService.class);
        localServiceStart.getManager().getImplementation().setPath(path);
        localServiceStart.getManager().getImplementation().setLocalService(localServiceRequires);


        localServiceEnd = ServiceFactory.makeLocalServiceFrom(SetLocationEndService.class);
        localServiceEnd.getManager().getImplementation().setPath(path);
        localServiceEnd.getManager().getImplementation().setLocalService(localServiceRequires);

        endAdapterComponent = new EndAdapterComponent();
        startAdapterComponent = new StartAdapterComponent();

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component used to transform two points into a Path",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceStart,localServiceEnd,localServiceRequires
                });
    }

    @Override
    public void removeLocalDevice(){
        UpnpServiceStore.getUpnpService().getRegistry().removeDevice(this.endAdapterComponent.getLocalDevice());
        UpnpServiceStore.getUpnpService().getRegistry().removeDevice(this.startAdapterComponent.getLocalDevice());
        super.removeLocalDevice();
    }

    @Override
    public void addLocalDevice(){
        UpnpServiceStore.addLocalDevice(this.endAdapterComponent.getLocalDevice());
        UpnpServiceStore.addLocalDevice(this.startAdapterComponent.getLocalDevice());
        super.addLocalDevice();
    }
}
