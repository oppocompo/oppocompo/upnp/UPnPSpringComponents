package com.irit.mapsender.model.components.pathTimer;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

@UpnpService(
        serviceId = @UpnpServiceId("DisplayPath"),
        serviceType = @UpnpServiceType(value="DisplayPath")
)
public class DisplayPathService extends Service {

    private final static double R = 6371.0088; //rayon de la terre
    private final static int nbOfKilometersForOnePercent = 3;

    @UpnpStateVariable
    private String startLongitude;

    @UpnpStateVariable
    private String startLatitude;

    @UpnpStateVariable
    private String endLongitude;

    @UpnpStateVariable
    private String endLatitude;

    private double haversineDistance(double lat1, double lon1, double lat2, double lon2) {
        double latDistance = toRad(lat2-lat1);
        double lonDistance = toRad(lon2-lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
                Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
                        Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        return R * c;
    }
    private Double toRad(Double value) {
        return value * Math.PI / 180;
    }

    private static double round(double value) {
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @UpnpAction(name = "DisplayPath")
    public void displayPathTo(
            @UpnpInputArgument(name = "StartLongitude") String startLongitude,
            @UpnpInputArgument(name = "StartLatitude") String startLatitude,
            @UpnpInputArgument(name = "EndLatitude") String endLatitude,
            @UpnpInputArgument(name = "EndLongitude") String endLongitude
    ) {
        JSONObject jsoninfos = new JSONObject();
        executeRequireAndThen("BatteryInfo","BatteryInfo",new HashMap<>(), actionInvocation -> {
            int batteryLevel = (int) actionInvocation.getOutput("BatteryLevel").getValue();
            double distance = round(haversineDistance(Double.parseDouble(startLatitude),Double.parseDouble(startLongitude),Double.parseDouble(endLatitude),Double.parseDouble(endLongitude)));
            jsoninfos.put("batteryLevel","Battery level is at " + batteryLevel +"%");
            jsoninfos.put("distance","Destination is " + distance + " kilometers away");
            if (distance/nbOfKilometersForOnePercent > batteryLevel) {
                jsoninfos.put("deviceChargedEnough","The battery is not charged enough to handle the whole trip");
            } else {
                jsoninfos.put("deviceChargedEnough","Battery is charged enough for the trip");
            };
            Map<String,Object> args = new HashMap<>();
            args.put("Infos",jsoninfos.toString());
            executeRequire("DisplayInfos","DisplayInfos",args);
        });
    }
}