package com.irit.mapsender.model.components.zoomLevel;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;

@UpnpService(
        serviceId =  @UpnpServiceId("ZoomLevel"),
        serviceType =  @UpnpServiceType("ZoomLevel")
)
public class TownZoomLevelService extends Service {

    @UpnpStateVariable
    private static final Float zoom = 10.f;

    @UpnpAction(name="ZoomLevel", out = @UpnpOutputArgument(name = "Zoom"))
    public Float getZoomLevel(){
        return zoom;
    }
}
