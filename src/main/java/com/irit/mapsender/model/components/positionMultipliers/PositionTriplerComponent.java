package com.irit.mapsender.model.components.positionMultipliers;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

@ParametrableComponent(
        requires = {"SetLocation", "SetLocation", "SetLocation"},
        provides = {"SetLocation"},
        description = "A component that takes a required SetLocation and send it to three differents services")
public class PositionTriplerComponent extends Component {

    private final static int VERSION = 1;
    private LocalService<SetLocationTriplerService> localService2 = null;
    private LocalService<DependencyInjectionService> localService = null;

    public PositionTriplerComponent(String name) {

        Map<String, ServiceId> map = new HashMap<>();
        map.put("SetLocation1",new UDAServiceId("SetLocation"));
        map.put("SetLocation2",new UDAServiceId("SetLocation"));
        map.put("SetLocation3",new UDAServiceId("SetLocation"));

        localService = ServiceFactory.makeDependencyInjectionService(map);
        localService2 = ServiceFactory.makeLocalServiceFrom(SetLocationTriplerService.class);
        localService2.getManager().getImplementation().setLocalService(localService);

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component that takes a required SetLocation and send it to three differents services",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localService,localService2
                });


    }
}
