package com.irit.mapsender.model.components.displayPathDoubler;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;

import java.util.HashMap;
import java.util.Map;

@UpnpService(
        serviceId = @UpnpServiceId("DisplayPaht"),
        serviceType = @UpnpServiceType("DisplayPath")
)
public class DisplayPathService extends Service {

    @UpnpStateVariable
    private String startLongitude;

    @UpnpStateVariable
    private String startLatitude;

    @UpnpStateVariable
    private String endLongitude;

    @UpnpStateVariable
    private String endLatitude;

    @UpnpAction(name = "DisplayPath")
    public void displayPathTo(
            @UpnpInputArgument(name = "StartLongitude") String startLongitude,
            @UpnpInputArgument(name = "StartLatitude") String startLatitude,
            @UpnpInputArgument(name = "EndLatitude") String endLatitude,
            @UpnpInputArgument(name = "EndLongitude") String endLongitude) {

        Map<String,Object> args = new HashMap<>();
        args.put("StartLatitutde",startLatitude);
        args.put("StartLongitude",startLongitude);
        args.put("EndLatitude",endLatitude);
        args.put("EndLongitude",endLongitude);

        executeRequire("DisplayPath1","DisplayPath",args);
        executeRequire("DisplayPath2","DisplayPath",args);
    }
}
