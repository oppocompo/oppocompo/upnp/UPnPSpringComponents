package com.irit.mapsender.model.components.positionToLocationPath;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

@ParametrableComponent(
        provides = {"SetLocation"},
        requires = {"SendPosition","DisplayPath"},
        description = "A component used to display a path from a SetLocation and SendPosition service"
)
public class positionToLocationComponent extends Component {

    private static final int VERSION = 1;
    private LocalService<SetLocationService> localServiceProvide = null;
    private LocalService<DependencyInjectionService> localServiceRequires = null;

    public positionToLocationComponent(String name) {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("SendPosition", new UDAServiceId("SendPosition"));
        map.put("DisplayPath", new UDAServiceId("DisplayPath"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);
        localServiceProvide = ServiceFactory.makeLocalServiceFrom(SetLocationService.class);
        localServiceProvide.getManager().getImplementation().setLocalService(localServiceRequires);

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component used to display a path from a SetLocation and SendPosition service",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceProvide,localServiceRequires
                });
    }
}
