package com.irit.mapsender.model.components.appendPoint;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

@ParametrableComponent(
        requires = {"DisplayPoints"},
        provides = {"SetLocation","DisplayPoints"},
        description = "A component used to add a point to a list of points"
)
public class AppendPointComponent extends Component {

    private static final int VERSION = 1;
    private LocalService<DependencyInjectionService> localServiceRequires;
    private LocalService<SetLocationService> localServiceSetLocation;
    private LocalService<DisplayPointsService> localServiceDisplayPoints;

    public AppendPointComponent(String name) {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("DisplayPoints", new UDAServiceId("DisplayPoints"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);
        localServiceSetLocation = ServiceFactory.makeLocalServiceFrom(SetLocationService.class);
        localServiceDisplayPoints = ServiceFactory.makeLocalServiceFrom(DisplayPointsService.class);

        localServiceSetLocation.getManager().getImplementation().setLocalService(localServiceRequires);
        localServiceSetLocation.getManager().getImplementation().setDisplayPointsService(localServiceDisplayPoints.getManager().getImplementation());
        localServiceDisplayPoints.getManager().getImplementation().setLocalService(localServiceRequires);
        localServiceDisplayPoints.getManager().getImplementation().setSetLocationService(localServiceSetLocation.getManager().getImplementation());

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component used to add a point to a list of points",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceRequires,localServiceDisplayPoints,localServiceSetLocation
                });
    }
}
