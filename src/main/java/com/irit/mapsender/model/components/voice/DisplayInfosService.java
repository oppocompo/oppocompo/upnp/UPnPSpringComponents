package com.irit.mapsender.model.components.voice;

import com.google.cloud.texttospeech.v1.*;
import com.google.protobuf.ByteString;
import com.irit.mapsender.model.components.Service;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import org.fourthline.cling.binding.annotations.*;
import org.json.JSONObject;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

@UpnpService(
        serviceId = @UpnpServiceId("DisplayInfos"),
        serviceType = @UpnpServiceType("DisplayInfos")
)
public class DisplayInfosService extends Service {

    private final static String FILE_PATH = new File("").getAbsolutePath().concat("/src/main/resources/output.mp3");
    private final static String LANGUAGE_CODE = "en-US";
    private final static SsmlVoiceGender VOICE_GENDER = SsmlVoiceGender.NEUTRAL;
    private final static AudioEncoding SOUND_FORMAT = AudioEncoding.MP3;
    @UpnpStateVariable
    private String infos;

    private void synthesizeText(String text) {
        // Instantiates a client
        try (TextToSpeechClient textToSpeechClient = TextToSpeechClient.create()) {
            // Set the text input to be synthesized
            SynthesisInput input = SynthesisInput.newBuilder().setText(text).build();

            // Build the voice request, select the language code ("en-US") and the ssml voice gender
            // ("neutral")
            VoiceSelectionParams voice =
                    VoiceSelectionParams.newBuilder()
                            .setLanguageCode(LANGUAGE_CODE)
                            .setSsmlGender(VOICE_GENDER)
                            .build();

            // Select the type of audio file you want returned
            AudioConfig audioConfig =
                    AudioConfig.newBuilder().setAudioEncoding(SOUND_FORMAT).build();

            // Perform the text-to-speech request on the text input with the selected voice parameters and
            // audio file type
            SynthesizeSpeechResponse response =
                    textToSpeechClient.synthesizeSpeech(input, voice, audioConfig);

            // Get the audio contents from the response
            ByteString audioContents = response.getAudioContent();

            // Write the response to the output file.
            try (OutputStream out = new FileOutputStream(FILE_PATH)) {
                out.write(audioContents.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readText() {
        if (Files.exists(Path.of(FILE_PATH))) {
            try {
                FileInputStream fis = new FileInputStream(FILE_PATH);
                Player player = new Player(fis);
                player.play();
            } catch (FileNotFoundException | JavaLayerException e) {
                e.printStackTrace();
            }
        }
    }

    @UpnpAction(name = "DisplayInfos")
    public void readInfos(@UpnpInputArgument(name="Infos") String infos) {
        String textToRead = "";

        JSONObject json = new JSONObject(infos);

        for (String key: json.keySet()) {
            textToRead = "";
            textToRead += key + " : \n";
            textToRead += json.getString(key)+"\n";
            synthesizeText(textToRead);
            readText();
        }
    }
}
