package com.irit.mapsender.model.components.pathConverter.adapters;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

public class StartAdapterComponent extends Component {

    private static final int VERSION = 1;
    private LocalService<SetLocationStartAdapterService> localServiceAdapter;
    private LocalService<DependencyInjectionService> localServiceRequires;

    public StartAdapterComponent() {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("SetLocationStart", new UDAServiceId("SetLocationStart"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);

        localServiceAdapter = ServiceFactory.makeLocalServiceFrom(SetLocationStartAdapterService.class);
        localServiceAdapter.getManager().getImplementation().setLocalService(localServiceRequires);

        localDevice = DeviceFactory.makeLocalDevice(
                "SetLocationStartAdapter",
                "A SetLocation to SetLocationStart adapter",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceAdapter,localServiceRequires
                });
    }
}
