package com.irit.mapsender.model.components.pathConverter;

import com.irit.mapsender.model.beans.Path;
import com.irit.mapsender.model.beans.Position;
import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;

import java.util.HashMap;
import java.util.Map;

@UpnpService(
        serviceId = @UpnpServiceId("SetLocationEnd"),
        serviceType = @UpnpServiceType("SetLocationEnd")
)
public class SetLocationEndService extends Service {

    private Path path;

    @UpnpStateVariable
    private String longitude;
    @UpnpStateVariable
    private String latitude;
    @UpnpStateVariable
    private String settings;

    public void setPath(Path path) {
        this.path = path;
    }

    @UpnpAction(name="SetLocationEnd")
    public void setLocation(@UpnpInputArgument(name = "Longitude") String longitude,
                            @UpnpInputArgument(name = "Latitude") String latitude,
                            @UpnpInputArgument(name = "Settings") String settings){
        this.longitude = longitude;
        this.latitude = latitude;
        this.settings = settings;

        Position position = new Position(Double.parseDouble(latitude),Double.parseDouble(longitude),settings);

        path.setEnd(position);

        if (path.pathIsComplete()){

            Map<String, Object> args = new HashMap<>();
            args.put("StartLongitude","" + path.getStart().getLng());
            args.put("StartLatitude","" + path.getStart().getLat());
            args.put("EndLongitude","" + path.getEnd().getLng());
            args.put("EndLatitude","" + path.getEnd().getLat());

            executeRequire("DisplayPath","DisplayPath",args);
        }
    }
}