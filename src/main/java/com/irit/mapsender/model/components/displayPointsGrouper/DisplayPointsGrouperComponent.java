package com.irit.mapsender.model.components.displayPointsGrouper;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import com.irit.mapsender.model.components.displayPointsGrouper.adapters.DisplayPointsFirstAdapterComponent;
import com.irit.mapsender.model.components.displayPointsGrouper.adapters.DisplayPointsSecondAdapterComponent;
import com.irit.stores.UpnpServiceStore;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

@ParametrableComponent(
        requires = {"DisplayPoints"},
        provides = {"DisplayPoints","DisplayPoints"},
        description = "A component used to join multiple DisplayPoints methods into a single DisplayPoints"
)
public class DisplayPointsGrouperComponent extends Component {

    private static final int VERSION = 1;
    private LocalService<DisplayPointsFirstService> localServiceFirst;
    private LocalService<DisplayPointsSecondService> localServiceSecond;

    private LocalService<DependencyInjectionService> localServiceRequires;

    private DisplayPointsFirstAdapterComponent displayPointsFirstAdapterComponent;
    private DisplayPointsSecondAdapterComponent displayPointsSecondAdapterComponent;


    public DisplayPointsGrouperComponent(String name) {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("DisplayPoints", new UDAServiceId("DisplayPoints"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);

        localServiceFirst = ServiceFactory.makeLocalServiceFrom(DisplayPointsFirstService.class);
        localServiceSecond = ServiceFactory.makeLocalServiceFrom(DisplayPointsSecondService.class);

        localServiceFirst.getManager().getImplementation().setDisplayPointsSecondService(localServiceSecond.getManager().getImplementation());
        localServiceSecond.getManager().getImplementation().setDisplayPointsFirstService(localServiceFirst.getManager().getImplementation());

        localServiceFirst.getManager().getImplementation().setLocalService(localServiceRequires);
        localServiceSecond.getManager().getImplementation().setLocalService(localServiceRequires);

        displayPointsFirstAdapterComponent = new DisplayPointsFirstAdapterComponent();
        displayPointsSecondAdapterComponent = new DisplayPointsSecondAdapterComponent();

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component used to join multiple DisplayPoints methods into one DisplayPoints",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceFirst,localServiceSecond,localServiceRequires
                });

    }

    @Override
    public void removeLocalDevice(){
        UpnpServiceStore.getUpnpService().getRegistry().removeDevice(this.displayPointsFirstAdapterComponent.getLocalDevice());
        UpnpServiceStore.getUpnpService().getRegistry().removeDevice(this.displayPointsSecondAdapterComponent.getLocalDevice());
        super.removeLocalDevice();
    }

    @Override
    public void addLocalDevice(){
        UpnpServiceStore.addLocalDevice(this.displayPointsFirstAdapterComponent.getLocalDevice());
        UpnpServiceStore.addLocalDevice(this.displayPointsSecondAdapterComponent.getLocalDevice());
        super.addLocalDevice();
    }
}
