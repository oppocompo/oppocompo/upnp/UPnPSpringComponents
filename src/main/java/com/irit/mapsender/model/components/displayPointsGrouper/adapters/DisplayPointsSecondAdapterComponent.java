package com.irit.mapsender.model.components.displayPointsGrouper.adapters;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

public class DisplayPointsSecondAdapterComponent extends Component {

    private static final int VERSION = 1;
    private LocalService<DisplayPointsSecondAdapterService> localServiceAdapter;
    private LocalService<DependencyInjectionService> localServiceRequires;

    public DisplayPointsSecondAdapterComponent() {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("DisplayPointsSecond", new UDAServiceId("DisplayPointsSecond"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);

        localServiceAdapter = ServiceFactory.makeLocalServiceFrom(DisplayPointsSecondAdapterService.class);
        localServiceAdapter.getManager().getImplementation().setLocalService(localServiceRequires);

        localDevice = DeviceFactory.makeLocalDevice(
                "DisplayPointsFirstAdapter",
                "A DisplayPoints to DisplayPointsSecond adapter",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceAdapter,localServiceRequires
                });
    }
}
