package com.irit.mapsender.model.components.displayPointsGrouper.adapters;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

public class DisplayPointsFirstAdapterComponent extends Component {

    private static final int VERSION = 1;
    private LocalService<DisplayPointsFirstAdapterService> localServiceAdapter;
    private LocalService<DependencyInjectionService> localServiceRequires;

    public DisplayPointsFirstAdapterComponent() {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("DisplayPointsFirst", new UDAServiceId("DisplayPointsFirst"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);

        localServiceAdapter = ServiceFactory.makeLocalServiceFrom(DisplayPointsFirstAdapterService.class);
        localServiceAdapter.getManager().getImplementation().setLocalService(localServiceRequires);

        localDevice = DeviceFactory.makeLocalDevice(
                "DisplayPointsFirstAdapter",
                "A DisplayPoints to DisplayPointsFirst adapter",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceAdapter,localServiceRequires
                });
    }
}
