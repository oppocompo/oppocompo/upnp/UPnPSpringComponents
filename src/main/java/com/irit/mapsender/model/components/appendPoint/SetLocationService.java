package com.irit.mapsender.model.components.appendPoint;

import com.irit.mapsender.model.beans.Position;
import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@UpnpService(
        serviceId = @UpnpServiceId("SetLocation"),
        serviceType = @UpnpServiceType("SetLocation")
)
public class SetLocationService extends Service {

    private DisplayPointsService displayPointsService;
    private String points;

    @UpnpStateVariable
    private String longitude;

    @UpnpStateVariable
    private String latitude;

    @UpnpStateVariable
    private String settings;

    public void setDisplayPointsService(DisplayPointsService displayPointsService) {
        this.displayPointsService = displayPointsService;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    @UpnpAction(name="SetLocation")
    public void setLocation(@UpnpInputArgument(name = "Longitude") String longitude,
                            @UpnpInputArgument(name = "Latitude") String latitude,
                            @UpnpInputArgument(name = "Settings") String settings) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.settings = settings;

        displayPointsService.setPosition(new Position(Double.valueOf(latitude),Double.valueOf(longitude),settings));

        JSONObject jsonPoint = new JSONObject();
        jsonPoint.put("longitude",longitude);
        jsonPoint.put("latitude",latitude);
        jsonPoint.put("settings",settings);

        JSONObject jsonPoints;
        JSONArray jsonArray;

        if (points == null) {
            jsonPoints = new JSONObject();
            jsonArray = new JSONArray();
        }else {
            jsonPoints = new JSONObject(points);
            jsonArray = new JSONArray(jsonPoints.getJSONArray("points"));
        }

        jsonArray.put(jsonPoint);
        jsonPoints.put("points",jsonArray);

        Map<String, Object> args = new HashMap<>();
        args.put("Points",jsonPoints.toString());

        executeRequire("DisplayPoints","DisplayPoints",args);
    }
}
