package com.irit.mapsender.model.components.nameGetters.town;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;

@UpnpService(
        serviceId = @UpnpServiceId("SetLocation"),
        serviceType = @UpnpServiceType("SetLocation")
)
public class TownSetLocationService extends Service {

    private TownNameService townNameService;

    @UpnpStateVariable
    private String longitude;
    @UpnpStateVariable
    private String latitude;
    @UpnpStateVariable
    private String settings;

    public void setNameService(TownNameService townNameService) { this.townNameService= townNameService; }

    @UpnpAction(name="SetLocation")
    public void setLocation(@UpnpInputArgument(name = "Longitude") String longitude,
                            @UpnpInputArgument(name = "Latitude") String latitude,
                            @UpnpInputArgument(name = "Settings") String settings){
        this.longitude = longitude;
        this.latitude = latitude;
        this.settings = settings;

        this.townNameService.setLongitude(longitude);
        this.townNameService.setLatitude(latitude);
    }
}
