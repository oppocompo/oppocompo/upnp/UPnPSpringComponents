package com.irit.mapsender.model.components.pointsAverage;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@UpnpService(
        serviceId = @UpnpServiceId("DisplayPoints"),
        serviceType = @UpnpServiceType("DisplayPoints")
)
public class DisplayPointsService extends Service {

    @UpnpStateVariable
    String points;

    @UpnpAction(name = "DisplayPoints")
    public void setLocation(@UpnpInputArgument(name = "Points") String points) {
        JSONObject json = new JSONObject(points);
        JSONArray jsonArray = json.getJSONArray("points");
        System.out.println(json);
        int cpt = 0;
        double averageLat = 0;
        double averageLng = 0;

        for (int i = 0 ; i < jsonArray.length() ; i++ ) {
            JSONObject jsonPoint = jsonArray.getJSONObject(i);
            cpt ++;
            String latitude;
            String longitude;
            try {
                latitude = jsonPoint.getString("latitude");
                longitude = jsonPoint.getString("longitude");
            } catch (JSONException e) {
                latitude = "0";
                longitude = "0";
                cpt--;
            }

            averageLat += Double.valueOf(latitude);
            averageLng += Double.valueOf(longitude);
        }

        averageLat /= cpt;
        averageLng /= cpt;

        Map<String, Object> args = new HashMap<>();
        args.put("Latitude",averageLat + "");
        args.put("Longitude",averageLng + "");
        args.put("Settings","{}");

        executeRequire("SetLocation","SetLocation",args);
    }
}
