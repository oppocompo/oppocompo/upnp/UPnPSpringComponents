package com.irit.mapsender.model.components.setZoom;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

@ParametrableComponent(
        requires = {"SetZoom", "SetLocation"},
        provides = {"SetZoom"},
        description = "A component used to change zoom level of a device"
)
public class SetZoomComponent extends Component {

    private final int VERSION = 1;
    private LocalService<SetZoomService> localService2 = null;
    private LocalService<DependencyInjectionService> localService = null;

    public SetZoomComponent(String name) {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("SetZoom", new UDAServiceId("SetZoom"));
        map.put("ZoomLevel",new UDAServiceId("ZoomLevel"));

        localService2 = ServiceFactory.makeLocalServiceFrom(SetZoomService.class);
        localService = ServiceFactory.makeDependencyInjectionService(map);
        localService2.getManager().getImplementation().setLocalService(localService);
        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component used to change zoom level of a device",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localService,localService2
                }
        );
    }
}
