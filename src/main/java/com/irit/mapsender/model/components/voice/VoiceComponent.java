package com.irit.mapsender.model.components.voice;

import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;

@ParametrableComponent(
        provides = {"DisplayInfos"},
        description = "A component used to read information with a speech synthesizer"
)
public class VoiceComponent extends Component {

    private final static int VERSION = 1;
    private LocalService<DisplayInfosService> localService;

    public VoiceComponent(String name) {
        localService = ServiceFactory.makeLocalServiceFrom(DisplayInfosService.class);

        localDevice = DeviceFactory.makeLocalDevice(
                "VoiceCompoent",
                "A component used to read information with a speech synthesizer",
                VERSION,
                "IRIT",
                new LocalService[]{localService});
    }
}
