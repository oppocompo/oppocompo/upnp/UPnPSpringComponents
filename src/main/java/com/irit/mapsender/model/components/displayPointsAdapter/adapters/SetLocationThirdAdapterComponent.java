package com.irit.mapsender.model.components.displayPointsAdapter.adapters;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

public class SetLocationThirdAdapterComponent extends Component {

    private static final int VERSION = 1;
    private LocalService<SetLocationThirdAdapterService> localServiceAdapter;
    private LocalService<DependencyInjectionService> localServiceRequires;

    public SetLocationThirdAdapterComponent() {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("SetLocationThird", new UDAServiceId("SetLocationThird"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);

        localServiceAdapter = ServiceFactory.makeLocalServiceFrom(SetLocationThirdAdapterService.class);
        localServiceAdapter.getManager().getImplementation().setLocalService(localServiceRequires);

        localDevice = DeviceFactory.makeLocalDevice(
                "SetLocationThirdAdapter",
                "A SetLocation to SetLocationThird adapter",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceAdapter,localServiceRequires
                });
    }
}
