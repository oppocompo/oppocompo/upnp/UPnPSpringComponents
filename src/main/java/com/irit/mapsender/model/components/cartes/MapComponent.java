package com.irit.mapsender.model.components.cartes;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.beans.Position;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@ParametrableComponent(
        requires = {"SetLocation"},
        provides = {},
        description = "A component that display a map used to send a position to another component",
        hasIHM = true,
        url = "/map"
)
public class MapComponent extends Component {

    public final String MY_SERVICE = "SetLocation";
    private final int VERSION = 1;
    private LocalService<DependencyInjectionService> localService = null;

    public MapComponent(String name){
        Map<String, ServiceId> map = new HashMap<>();
        map.put(MY_SERVICE, new UDAServiceId(MY_SERVICE));

        localService = ServiceFactory.makeDependencyInjectionService(map);
        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component that display a map used to send a position to another component",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localService
                });
    }

    public void callSetLocation(Position p) {
        Map<String, Object> args = new HashMap<>();
        args.put("Longitude", "" + p.getLng());
        args.put("Latitude", "" + p.getLat());
        args.put("Settings", new JSONObject().toString());
        localService.getManager().getImplementation().getRequired().get("SetLocation").execute(
                "SetLocation",
                args
        );
    }
}
