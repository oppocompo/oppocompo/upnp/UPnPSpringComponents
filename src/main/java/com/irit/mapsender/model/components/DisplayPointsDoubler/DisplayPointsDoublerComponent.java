package com.irit.mapsender.model.components.DisplayPointsDoubler;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

@ParametrableComponent(
        requires = {"DisplayPoints", "DisplayPoints"},
        provides = {"DisplayPoints"},
        description = "A component that takes a DisplayPoints service and send it to two other components"
)
public class DisplayPointsDoublerComponent extends Component {

    private static final int VERSION = 1;
    private LocalService<DisplayPointsService> localServiceProvides;
    private LocalService<DependencyInjectionService> localServiceRequires;

    public DisplayPointsDoublerComponent(String name) {

        Map<String, ServiceId> map = new HashMap<>();
        map.put("DisplayPoints1", new UDAServiceId("DisplayPoints"));
        map.put("DisplayPoints2", new UDAServiceId("DisplayPoints"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);
        localServiceProvides = ServiceFactory.makeLocalServiceFrom(DisplayPointsService.class);
        localServiceProvides.getManager().getImplementation().setLocalService(localServiceRequires);

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component that takes a DisplayPoints service and send it to two other components",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceProvides,localServiceRequires
                });

    }
}
