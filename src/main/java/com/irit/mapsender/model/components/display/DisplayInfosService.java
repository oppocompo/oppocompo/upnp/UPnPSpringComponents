package com.irit.mapsender.model.components.display;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;
import org.json.JSONObject;

@UpnpService(
        serviceId = @UpnpServiceId("DisplayInfos"),
        serviceType = @UpnpServiceType("DisplayInfos")
)
public class DisplayInfosService extends Service {

    @UpnpStateVariable
    private String infos;

    public DisplayInfosService() {
        JSONObject json = new JSONObject();
        json.put("empty","Aucune information n'est actuellement disponible.");
        this.infos = json.toString();
    }

    @UpnpAction(name = "DisplayInfos")
    public void displayInfos(@UpnpInputArgument(name="Infos") String infos) {

        this.infos = infos;
        System.out.println(this.infos);
    }

    public String getInfos() {
        return this.infos;
    }
}
