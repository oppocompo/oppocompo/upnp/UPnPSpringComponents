package com.irit.mapsender.model.components.display;

import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import com.irit.mapsender.model.components.DisplayableComponent;
import org.fourthline.cling.model.meta.LocalService;

@ParametrableComponent(
        provides = {"DisplayInfos"},
        description = "A component that display informations on screen",
        hasIHM = true,
        url = "/display"
)
public class DisplayComponent extends Component implements DisplayableComponent {

    private static int VERSION = 1;
    private LocalService<DisplayInfosService> localService;

    public DisplayComponent(String name) {
        localService = ServiceFactory.makeLocalServiceFrom(DisplayInfosService.class);

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component that display informations on screen",
                VERSION,
                "IRIT",
                new LocalService[]{localService});
    }

    @Override
    public String displayInfos() {
        return localService.getManager().getImplementation().getInfos();
    }
}
