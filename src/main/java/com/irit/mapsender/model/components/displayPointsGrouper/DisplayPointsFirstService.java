package com.irit.mapsender.model.components.displayPointsGrouper;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@UpnpService(
        serviceId =  @UpnpServiceId("DisplayPointsFirst"),
        serviceType = @UpnpServiceType("DisplayPointsFirst")
)
public class DisplayPointsFirstService extends Service {

    private DisplayPointsSecondService displayPointsSecondService;

    @UpnpStateVariable
    private String points;

    public String getPoints() {
        return points;
    }


    public void setDisplayPointsSecondService(DisplayPointsSecondService displayPointsSecondService) {
        this.displayPointsSecondService = displayPointsSecondService;
    }

    @UpnpAction(name = "DisplayPointsFirst")
    public void displayPointsFirst(@UpnpInputArgument(name = "Points") String points) {

        this.points = points;

        JSONObject json = new JSONObject(points);
        JSONArray currentPoints = new JSONArray(json.getJSONArray("points"));

        if (displayPointsSecondService.getPoints() != null) {

            JSONObject otherJson = new JSONObject(displayPointsSecondService.getPoints());
            JSONArray otherPointsArray = new JSONArray(otherJson.getJSONArray("points"));
            for (int i = 0; i < otherPointsArray.length(); i++) {
                currentPoints.put(otherPointsArray.getJSONObject(i));
            }

            JSONObject jsonToSend = new JSONObject();
            jsonToSend.put("points",currentPoints);

            Map<String,Object> args = new HashMap<>();
            args.put("Points", jsonToSend.toString());

            executeRequire("DisplayPoints","DisplayPoints",args);
        }
    }

}
