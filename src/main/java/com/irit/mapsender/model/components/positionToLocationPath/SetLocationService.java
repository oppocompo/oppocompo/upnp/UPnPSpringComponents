package com.irit.mapsender.model.components.positionToLocationPath;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@UpnpService(
        serviceId = @UpnpServiceId("SetLocation"),
        serviceType = @UpnpServiceType("SetLocation")
)
public class SetLocationService extends Service {

    @UpnpStateVariable
    private String latitude;
    @UpnpStateVariable
    private String longitude;
    @UpnpStateVariable
    private String settings;

    @UpnpAction(name = "SetLocation")
    public void setLocation(@UpnpInputArgument(name="Latitude") String latitude,
                            @UpnpInputArgument(name="Longitude") String longitude,
                            @UpnpInputArgument(name="Settings") String settings) {
        Map<String, Object> args = new HashMap<>();
        args.put("EndLatitude", latitude);
        args.put("EndLongitude", longitude);

        executeRequireAndThen("SendPosition","SendPosition",new HashMap(),actionInvocation -> {
            String position = actionInvocation.getOutput("Position").toString();
            JSONObject json = new JSONObject(position);

            args.put("StartLongitude", json.get("longitude").toString());
            args.put("StartLatitude", json.get("latitude").toString());
            executeRequire("DisplayPath","DisplayPath",args);
        });
    }

}
