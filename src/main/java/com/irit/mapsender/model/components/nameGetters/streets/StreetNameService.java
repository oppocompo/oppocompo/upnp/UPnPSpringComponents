package com.irit.mapsender.model.components.nameGetters.streets;

import com.irit.mapsender.model.components.Service;
import com.irit.mapsender.model.tools.ApiClient;
import org.fourthline.cling.binding.annotations.*;

@UpnpService(
        serviceId = @UpnpServiceId("Name"),
        serviceType = @UpnpServiceType("Name")
)
public class StreetNameService extends Service {

    private ApiClient apiClient;
    private String latitude;
    private String longitude;

    @UpnpStateVariable
    private String name;

    public StreetNameService(){ apiClient = new ApiClient(); }

    public void setLatitude(String latitude) { this.latitude = latitude; }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @UpnpAction(name="Name",out=@UpnpOutputArgument(name="Name"))
    public String getCountryName() {
        return apiClient.getStreetName(latitude,longitude);
    }
}
