package com.irit.mapsender.model.components.nameGetters.streets;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;

@UpnpService(
        serviceId = @UpnpServiceId("SetLocation"),
        serviceType = @UpnpServiceType("SetLocation")
)
public class StreetSetLocationService extends Service {

    private StreetNameService streetNameService;

    @UpnpStateVariable
    private String longitude;
    @UpnpStateVariable
    private String latitude;
    @UpnpStateVariable
    private String settings;

    public void setNameService(StreetNameService streetNameService) { this.streetNameService = streetNameService; }

    @UpnpAction(name="SetLocation")
    public void setLocation(@UpnpInputArgument(name = "Longitude") String longitude,
                            @UpnpInputArgument(name = "Latitude") String latitude,
                            @UpnpInputArgument(name = "Settings") String settings){
        this.longitude = longitude;
        this.latitude = latitude;
        this.settings = settings;

        this.streetNameService.setLongitude(longitude);
        this.streetNameService.setLatitude(latitude);
    }
}
