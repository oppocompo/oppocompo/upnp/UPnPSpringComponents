package com.irit.mapsender.model.components;

public interface DisplayableComponent {

    public String displayInfos();
}
