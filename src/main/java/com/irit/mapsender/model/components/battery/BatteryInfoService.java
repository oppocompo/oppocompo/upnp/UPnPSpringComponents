package com.irit.mapsender.model.components.battery;

import com.irit.mapsender.model.components.Service;
import org.apache.commons.lang.SystemUtils;
import org.fourthline.cling.binding.annotations.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@UpnpService(
        serviceId = @UpnpServiceId("BatteryInfo"),
        serviceType = @UpnpServiceType("BatteryInfo"))
public class BatteryInfoService extends Service {

    @UpnpStateVariable
    private int batteryLevel = -1;

    @UpnpAction(name="BatteryInfo", out= @UpnpOutputArgument(name="BatteryLevel"))
    public int batteryInfo() {
        try {
            if (SystemUtils.IS_OS_UNIX) {
                String cmd = "cat /sys/class/power_supply/BAT0/capacity";
                Runtime run = Runtime.getRuntime();
                Process pr = run.exec(cmd);
                pr.waitFor();
                BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
                String line = "";
                String output = "";
                while ((line = buf.readLine()) != null) {
                    output += line;
                }
                try {
                    this.batteryLevel = Integer.valueOf(output);
                }catch (Exception e) {
                    //si on arrive ici, c'est que le PC n'as pas de batterie (ex : desktop)
                    this.batteryLevel = 100;
                }
            }else {
                //TODO gérer le cas si le PC tourne sous Windows
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.batteryLevel;
    }
}
