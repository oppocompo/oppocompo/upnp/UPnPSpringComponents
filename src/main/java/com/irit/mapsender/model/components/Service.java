package com.irit.mapsender.model.components;

import com.irit.dependencyinjection.DependencyInjectionService;
import org.fourthline.cling.model.action.ActionInvocation;
import org.fourthline.cling.model.meta.LocalService;

import java.util.Map;
import java.util.function.Consumer;

public abstract class Service {

    protected LocalService<DependencyInjectionService> localService;

    public void setLocalService(LocalService<DependencyInjectionService> localService) {
        this.localService = localService;
    }

    protected void executeRequire(String component, String method, Map arguments) {
        localService.getManager().getImplementation().getRequired().get(component).execute(
                method,
                arguments
        );
    }

    protected void executeRequireAndThen(String component, String method, Map arguments, Consumer<ActionInvocation> afterExecute) {
        localService.getManager().getImplementation().getRequired().get(component).execute(
                method,
                arguments,
                afterExecute);
    }
}
