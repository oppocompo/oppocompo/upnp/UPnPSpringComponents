package com.irit.mapsender.model.components.displayPointsAdapter;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


@UpnpService(
        serviceId = @UpnpServiceId("SetLocationFirst"),
        serviceType = @UpnpServiceType("SetLocationFirst")
)
public class SetLocationFirstService extends Service {

    private final static int ARRAY_POSITION = 0;
    private JSONObject json;

    @UpnpStateVariable
    private String longitude;
    @UpnpStateVariable
    private String latitude;
    @UpnpStateVariable
    private String settings;


    public void setJson(JSONObject json) { this.json = json; }

    @UpnpAction(name="SetLocationFirst")
    public void setLocation(@UpnpInputArgument(name = "Longitude") String longitude,
                            @UpnpInputArgument(name = "Latitude") String latitude,
                            @UpnpInputArgument(name = "Settings") String settings){
        this.longitude = longitude;
        this.latitude = latitude;
        this.settings = settings;

        JSONObject jsonPosition = new JSONObject();
        jsonPosition.put("longitude",longitude);
        jsonPosition.put("latitude",latitude);
        jsonPosition.put("settings",settings);

        JSONArray listePoints = json.getJSONArray("points");
        listePoints.put(ARRAY_POSITION,jsonPosition);
        json.put("points",listePoints);

        Map<String, Object> args = new HashMap<>();
        args.put("Points", json.toString());

        executeRequire("DisplayPoints","DisplayPoints",args);
    }
}
