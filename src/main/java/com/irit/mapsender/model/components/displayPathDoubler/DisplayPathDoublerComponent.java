package com.irit.mapsender.model.components.displayPathDoubler;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

@ParametrableComponent(
        provides = {"DisplayPath"},
        requires = {"DisplayPath","DisplayPath"},
        description = "A component that send a DisplayPath Service to two differents components"
)
public class DisplayPathDoublerComponent extends Component {

    private static final int VERSION = 1;
    private LocalService<DisplayPathService> localServiceProvides;
    private LocalService<DependencyInjectionService> localServiceRequires;

    public DisplayPathDoublerComponent(String name) {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("DisplayPath1", new UDAServiceId("DisplayPath"));
        map.put("DisplayPath2", new UDAServiceId("DisplayPath"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);
        localServiceProvides = ServiceFactory.makeLocalServiceFrom(DisplayPathService.class);
        localServiceProvides.getManager().getImplementation().setLocalService(localServiceRequires);

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component that send a DisplayPath Service to two differents components",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceProvides,localServiceRequires
                });
    }
}
