package com.irit.mapsender.model.components;

import com.irit.stores.UpnpServiceStore;
import org.fourthline.cling.model.meta.LocalDevice;

public abstract class Component {

    protected Boolean isVisible = false;
    protected LocalDevice localDevice;

    public void removeLocalDevice() {
        if (isVisible) {
            UpnpServiceStore.getUpnpService().getRegistry().removeDevice(this.localDevice);
            isVisible = false;
        }
    }

    public void addLocalDevice() {
        if (!isVisible) {
            UpnpServiceStore.addLocalDevice(
                    this.localDevice
            );
            isVisible = true;
        }
    }

    public LocalDevice getLocalDevice() {
        return localDevice;
    }

    public boolean isOn() {
        return isVisible;
    }
}
