package com.irit.mapsender.model.components.DisplayPointsDoubler;

import com.irit.dependencyinjection.DependencyInjectionService;
import org.fourthline.cling.binding.annotations.*;
import org.fourthline.cling.model.meta.LocalService;

import java.util.HashMap;
import java.util.Map;

@UpnpService(
        serviceId = @UpnpServiceId("DisplayPoints"),
        serviceType = @UpnpServiceType("DisplayPoints")
)
public class DisplayPointsService {

    @UpnpStateVariable
    private String points;

    private LocalService<DependencyInjectionService> localService = null;

    public void setLocalService(LocalService<DependencyInjectionService> localService) {
        this.localService = localService;
    }

    @UpnpAction(name="DisplayPoints")
    public void setLocation(@UpnpInputArgument(name = "Points") String points){

        this.points = points;
        Map<String, Object> args = new HashMap<>();
        args.put("Points",points);
        localService.getManager().getImplementation().getRequired().get("DisplayPoints1").execute(
                "DisplayPoints",
                args);
        localService.getManager().getImplementation().getRequired().get("DisplayPoints2").execute(
                "DisplayPoints",
                args);
    }
}
