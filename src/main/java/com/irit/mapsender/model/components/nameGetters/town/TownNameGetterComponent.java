package com.irit.mapsender.model.components.nameGetters.town;

import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;

@ParametrableComponent(
        provides = {"SetLocation", "Name"},
        description = "A component that return the name of a town from a location"
)
public class TownNameGetterComponent extends Component {

    private final int VERSION = 1;
    private LocalService<TownSetLocationService> localServiceSetLocation;
    private LocalService<TownNameService> localServiceName;

    public TownNameGetterComponent(String name) {
        localServiceSetLocation = ServiceFactory.makeLocalServiceFrom(TownSetLocationService.class);
        localServiceName = ServiceFactory.makeLocalServiceFrom(TownNameService.class);
        localServiceSetLocation.getManager().getImplementation().setNameService(localServiceName.getManager().getImplementation());

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component that return the name of a town from a location",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceName,localServiceSetLocation
                });
    }

}
