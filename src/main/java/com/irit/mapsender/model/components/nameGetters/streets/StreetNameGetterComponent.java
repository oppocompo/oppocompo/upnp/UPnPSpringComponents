package com.irit.mapsender.model.components.nameGetters.streets;

import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;

@ParametrableComponent(
        provides = {"SetLocation", "Name"},
        description = "Un composant qui envoie une méthode pour lui donner une " +
                "position et une méthode pour renvoyer la rue lié à la position. ")
public class StreetNameGetterComponent extends Component {

    private final int VERSION = 1;
    private LocalService<StreetSetLocationService> localServiceSetLocation;
    private LocalService<StreetNameService> localServiceName;

    public StreetNameGetterComponent(String name) {
        localServiceSetLocation = ServiceFactory.makeLocalServiceFrom(StreetSetLocationService.class);
        localServiceName = ServiceFactory.makeLocalServiceFrom(StreetNameService.class);
        localServiceSetLocation.getManager().getImplementation().setNameService(localServiceName.getManager().getImplementation());

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component that return the name of the road from a location",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceName,localServiceSetLocation
                });
    }
}
