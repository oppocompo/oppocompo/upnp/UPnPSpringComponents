package com.irit.mapsender.model.components.pathConverter.adapters;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

public class EndAdapterComponent extends Component {

    private static final int VERSION = 1;
    private LocalService<SetLocationEndAdapterService> localServiceAdapter;
    private LocalService<DependencyInjectionService> localServiceRequires;

    public EndAdapterComponent() {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("SetLocationEnd", new UDAServiceId("SetLocationEnd"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);

        localServiceAdapter = ServiceFactory.makeLocalServiceFrom(SetLocationEndAdapterService.class);
        localServiceAdapter.getManager().getImplementation().setLocalService(localServiceRequires);

        localDevice = DeviceFactory.makeLocalDevice(
                "SetLocationEndAdapter",
                "A SetLocation to SetLocationEnd adapter",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceAdapter,localServiceRequires
                });
    }
}
