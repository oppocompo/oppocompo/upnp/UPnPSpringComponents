package com.irit.mapsender.model.components.appendPoint;

import com.irit.mapsender.model.beans.Position;
import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@UpnpService(
        serviceId = @UpnpServiceId("DisplayPoints"),
        serviceType = @UpnpServiceType("DisplayPoints")
)
public class DisplayPointsService extends Service {

    @UpnpStateVariable
    private String points;

    private Position position;
    private SetLocationService setLocationService;

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setSetLocationService(SetLocationService setLocationService) {
        this.setLocationService = setLocationService;
    }

    @UpnpAction(name="DisplayPoints")
    public void displayPoints(@UpnpInputArgument(name = "Points") String points) {

        this.points = points;
        setLocationService.setPoints(points);

        JSONObject pointsJson = new JSONObject(points);
        JSONArray pointsArray = new JSONArray(pointsJson.getJSONArray("points"));

        if (position != null) {
            JSONObject pointJson = new JSONObject();
            pointJson.put("latitude",position.getLat()+"");
            pointJson.put("longitude", position.getLng()+"");
            pointJson.put("settings", position.getSettings());
            pointsArray.put(pointJson);
        }

        pointsJson.put("points",pointsArray);

        Map<String, Object> args = new HashMap<>();
        args.put("Points",pointsJson.toString());

        executeRequire("DisplayPoints","DisplayPoints",args);
    }
}
