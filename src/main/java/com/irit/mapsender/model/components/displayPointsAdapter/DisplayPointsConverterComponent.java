package com.irit.mapsender.model.components.displayPointsAdapter;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import com.irit.mapsender.model.components.displayPointsAdapter.adapters.SetLocationFirstAdapterComponent;
import com.irit.mapsender.model.components.displayPointsAdapter.adapters.SetLocationSecondAdapterComponent;
import com.irit.mapsender.model.components.displayPointsAdapter.adapters.SetLocationThirdAdapterComponent;
import com.irit.stores.UpnpServiceStore;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@ParametrableComponent(
        requires = {"SetLocation", "SetLocation", "SetLocation"},
        provides = {"DisplayPoints"},
        description = "A component that can use up to three points in a DisplayPoints method")
public class DisplayPointsConverterComponent extends Component {

    private static final int VERSION = 1;
    private LocalService<SetLocationFirstService> localServiceFirst;
    private LocalService<SetLocationSecondService> localServiceSecond;
    private LocalService<SetLocationThirdService> localServiceThird;
    JSONObject json;

    private LocalService<DependencyInjectionService> localServiceRequires;

    private SetLocationFirstAdapterComponent setLocationFirstAdapterComponent;
    private SetLocationSecondAdapterComponent setLocationSecondAdapterComponent;
    private SetLocationThirdAdapterComponent setLocationThirdAdapterComponent;

    public DisplayPointsConverterComponent(String name) {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("DisplayPoints", new UDAServiceId("DisplayPoints"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);

        initJsonObject();

        localServiceFirst = ServiceFactory.makeLocalServiceFrom(SetLocationFirstService.class);
        localServiceFirst.getManager().getImplementation().setJson(json);
        localServiceFirst.getManager().getImplementation().setLocalService(localServiceRequires);

        localServiceSecond = ServiceFactory.makeLocalServiceFrom(SetLocationSecondService.class);
        localServiceSecond.getManager().getImplementation().setJson(json);
        localServiceSecond.getManager().getImplementation().setLocalService(localServiceRequires);

        localServiceThird = ServiceFactory.makeLocalServiceFrom(SetLocationThirdService.class);
        localServiceThird.getManager().getImplementation().setJson(json);
        localServiceThird.getManager().getImplementation().setLocalService(localServiceRequires);

        setLocationFirstAdapterComponent = new SetLocationFirstAdapterComponent();
        setLocationSecondAdapterComponent = new SetLocationSecondAdapterComponent();
        setLocationThirdAdapterComponent = new SetLocationThirdAdapterComponent();

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component that can use up to three points in a DisplayPoints method",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceFirst,localServiceSecond,localServiceThird,localServiceRequires
                });

    }

    private void initJsonObject(){
        json = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject emptyPoint = new JSONObject();
        emptyPoint.put("Latitude","");
        emptyPoint.put("Longitude","");
        emptyPoint.put("Settings","");
        array.put(emptyPoint);
        array.put(emptyPoint);
        array.put(emptyPoint);
        json.put("points",array);
    }

    @Override
    public void removeLocalDevice(){
        UpnpServiceStore.getUpnpService().getRegistry().removeDevice(this.setLocationFirstAdapterComponent.getLocalDevice());
        UpnpServiceStore.getUpnpService().getRegistry().removeDevice(this.setLocationSecondAdapterComponent.getLocalDevice());
        UpnpServiceStore.getUpnpService().getRegistry().removeDevice(this.setLocationThirdAdapterComponent.getLocalDevice());
        super.removeLocalDevice();
    }

    @Override
    public void addLocalDevice(){
        UpnpServiceStore.addLocalDevice(this.setLocationFirstAdapterComponent.getLocalDevice());
        UpnpServiceStore.addLocalDevice(this.setLocationSecondAdapterComponent.getLocalDevice());
        UpnpServiceStore.addLocalDevice(this.setLocationThirdAdapterComponent.getLocalDevice());
        super.addLocalDevice();
    }
}
