package com.irit.mapsender.model.components.displayPointsAdapter.adapters;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

public class SetLocationFirstAdapterComponent extends Component {

    private static final int VERSION = 1;
    private LocalService<SetLocationFirstAdapterService> localServiceAdapter;
    private LocalService<DependencyInjectionService> localServiceRequires;

    public SetLocationFirstAdapterComponent() {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("SetLocationFirst", new UDAServiceId("SetLocationFirst"));

        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);

        localServiceAdapter = ServiceFactory.makeLocalServiceFrom(SetLocationFirstAdapterService.class);
        localServiceAdapter.getManager().getImplementation().setLocalService(localServiceRequires);

        localDevice = DeviceFactory.makeLocalDevice(
                "SetLocationFirstAdapter",
                "A SetLocation to SetLocationFirst adapter",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localServiceAdapter,localServiceRequires
                });
    }
}
