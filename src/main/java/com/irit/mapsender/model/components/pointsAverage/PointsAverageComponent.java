package com.irit.mapsender.model.components.pointsAverage;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

@ParametrableComponent(
        requires = {"SetLocation"},
        provides = {"DisplayPoints"},
        description = "A component used to calculate an average position from multiples points"
)
public class PointsAverageComponent extends Component {

    private final int VERSION = 1;
    private LocalService<DisplayPointsService> localServiceProvide;
    private LocalService<DependencyInjectionService> localServiceRequires;

    public PointsAverageComponent(String name) {

        Map<String, ServiceId> map = new HashMap<>();
        map.put("SetLocation",new UDAServiceId("SetLocation"));
        localServiceRequires = ServiceFactory.makeDependencyInjectionService(map);

        localServiceProvide = ServiceFactory.makeLocalServiceFrom(DisplayPointsService.class);
        localServiceProvide.getManager().getImplementation().setLocalService(localServiceRequires);

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component used to calculate an average position from multiples points",
                VERSION,
                "IRIT",
                new LocalService[] {
                   localServiceRequires,localServiceProvide
                });
    }
}
