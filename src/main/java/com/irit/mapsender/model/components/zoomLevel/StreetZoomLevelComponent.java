package com.irit.mapsender.model.components.zoomLevel;

import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.mapsender.model.annotations.ParametrableComponent;
import com.irit.mapsender.model.components.Component;
import org.fourthline.cling.model.meta.LocalService;

@ParametrableComponent(
        provides = {"ZoomLevel"},
        description = "A component that send a zoom level corresponding to a street"
)
public class StreetZoomLevelComponent extends Component {

    private static final int VERSION = 1;
    private static LocalService<StreetZoomLevelService> localService = null;

    public StreetZoomLevelComponent(String name) {
        localService = ServiceFactory.makeLocalServiceFrom(StreetZoomLevelService.class);

        localDevice = DeviceFactory.makeLocalDevice(
                name,
                "A component that send a zoom level corresponding to a street",
                VERSION,
                "IRIT",
                new LocalService[]{
                        localService
                });
    }

}
