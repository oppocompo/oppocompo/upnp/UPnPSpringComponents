package com.irit.mapsender.model.components.displayPointsAdapter.adapters;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;

import java.util.HashMap;
import java.util.Map;

@UpnpService(
        serviceId = @UpnpServiceId("SetLocation"),
        serviceType = @UpnpServiceType("SetLocation")
)
public class SetLocationFirstAdapterService extends Service {

    @UpnpStateVariable
    private String longitude;
    @UpnpStateVariable
    private String latitude;
    @UpnpStateVariable
    private String settings;

    @UpnpAction(name="SetLocation")
    public void setLocation(@UpnpInputArgument(name = "Longitude") String longitude,
                            @UpnpInputArgument(name = "Latitude") String latitude,
                            @UpnpInputArgument(name = "Settings") String settings){
        this.longitude = longitude;
        this.latitude = latitude;
        this.settings = settings;

        Map<String, Object> args = new HashMap<>();
        args.put("Latitude",this.latitude);
        args.put("Longitude",this.longitude);
        args.put("Settings",this.settings);
        executeRequire("SetLocationFirst","SetLocationFirst",args);
    }
}
