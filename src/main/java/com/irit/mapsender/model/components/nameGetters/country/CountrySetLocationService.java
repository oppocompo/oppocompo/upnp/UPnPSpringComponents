package com.irit.mapsender.model.components.nameGetters.country;

import com.irit.mapsender.model.components.Service;
import org.fourthline.cling.binding.annotations.*;

@UpnpService(
        serviceId = @UpnpServiceId("SetLocation"),
        serviceType = @UpnpServiceType("SetLocation")
)
public class CountrySetLocationService extends Service {

    private CountyNameService countyNameService;

    @UpnpStateVariable
    private String longitude;
    @UpnpStateVariable
    private String latitude;
    @UpnpStateVariable
    private String settings;

    public void setNameService(CountyNameService countyNameService) {
        this.countyNameService = countyNameService;
    }

    @UpnpAction(name="SetLocation")
    public void setLocation(@UpnpInputArgument(name = "Longitude") String longitude,
                            @UpnpInputArgument(name = "Latitude") String latitude,
                            @UpnpInputArgument(name = "Settings") String settings){
        this.longitude = longitude;
        this.latitude = latitude;
        this.settings = settings;

        this.countyNameService.setLongitude(longitude);
        this.countyNameService.setLatitude(latitude);
    }
}
