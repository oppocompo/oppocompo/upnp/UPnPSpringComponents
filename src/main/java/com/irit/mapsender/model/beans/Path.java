package com.irit.mapsender.model.beans;

public class Path {

    private Position start;
    private Position end;

    public Path(){
        start = null;
        end = null;
    }

    public Position getStart() {
        return start;
    }

    public void setStart(Position start) {
        this.start = start;
    }

    public Position getEnd() {
        return end;
    }

    public void setEnd(Position end) {
        this.end = end;
    }

    public boolean pathIsComplete(){
        if (start != null && end != null){
            return true;
        }else{
            return false;
        }
    }
}
