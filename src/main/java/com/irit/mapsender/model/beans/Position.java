package com.irit.mapsender.model.beans;

public class Position {

    private double lat;
    private double lng;
    private String settings;

    public Position(double lat, double lng, String settings) {
        this.lat = lat;
        this.lng = lng;
        this.settings = settings;
    }

    public String getSettings() {
        return settings;
    }

    public void setSettings(String settings) {
        this.settings = settings;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "Position{" +
                "lat=" + lat +
                ", lng=" + lng +
                ", zoomLevel=" + settings +
                '}';
    }
}
