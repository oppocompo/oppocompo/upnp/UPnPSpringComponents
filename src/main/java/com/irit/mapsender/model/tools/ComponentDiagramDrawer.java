package com.irit.mapsender.model.tools;

import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.SourceStringReader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.stream.Stream;

public class ComponentDiagramDrawer {

    public static final String FILE_PATH = new File("").getAbsolutePath().concat("/src/main/resources/images/");
    public static final String EXTENSION = ".svg";
    private static final String START_UML = "@startuml\n";
    private static final String END_UML = "@enduml";

    public static void drawComponent(String[] requires, String[] provides, String componentName) {
        String source = START_UML;
        if (provides == null && requires == null) {
            source += "[" + componentName + "]\n";
        }

        String[] allServices = Stream.concat(Arrays.stream(provides), Arrays.stream(requires)).toArray(String[]::new);
        source += makeInterfacesText(allServices);

        int cpt = 0;

        for (String provide : provides) {
            source += provide+ cpt + " -down- [" + componentName + "]\n";
            cpt++;
        }
        for (String require : requires) {
            source += "[" + componentName + "] ..> " + require + cpt + " : requires\n";
            cpt++;
        }
        source += END_UML;

        SourceStringReader reader = new SourceStringReader(source);

        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        // Write the first image to "os"
        try {
            String desc = reader.generateImage(os, new FileFormatOption(FileFormat.SVG));
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // The XML is stored into svg
        final String svg = new String(os.toByteArray(), Charset.forName("UTF-8"));

        FileWriter fw = null;
        try {
            fw = new java.io.FileWriter(FILE_PATH+componentName+EXTENSION);
            fw.write(svg);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String makeInterfacesText(String[] services) {
        int cpt = 0;
        String interfaces = "";

        for (String service : services) {
            interfaces += "interface \"" + service + "\" as " + service+cpt +  "\n";
            cpt++;
        }

        return interfaces;
    }
}
