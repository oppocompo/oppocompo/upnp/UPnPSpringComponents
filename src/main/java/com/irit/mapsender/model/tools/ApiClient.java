package com.irit.mapsender.model.tools;

import org.json.JSONObject;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class ApiClient {

    private static final String OPENCAGEDATA_API_KEY = "a3fef811b44b44bcb3ec27aac2dafaca";
    HttpClient client;
    HttpRequest request;


    public ApiClient(){
        client = HttpClient.newHttpClient();
    }

    private void prepareRequest(String latitude, String longitude) {
        request = HttpRequest.newBuilder(
                URI.create("https://api.opencagedata.com/geocode/v1/json?q=" +
                        latitude +
                        "%2C" +
                        longitude +
                        "&key=" +
                        OPENCAGEDATA_API_KEY+
                        "&pretty=1"))
                .header("accept","application/json")
                .build();
    }

    public String getCountryName(String latitude, String longitude) {
        prepareRequest(latitude,longitude);
        String name;
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofString());
            JSONObject json = new JSONObject(response.body());
            name = json.getJSONArray("results").getJSONObject(0).getJSONObject("components").getString("country");
        } catch (Exception e) {
            name = "Pays inconnu";
        }
        return name;
    }

    public String getTownName(String latitude, String longitude) {
        prepareRequest(latitude,longitude);
        String name;
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofString());
            JSONObject json = new JSONObject(response.body());
            name = json.getJSONArray("results").getJSONObject(0).getJSONObject("components").getString("city");
        } catch (Exception e) {
            name = "Ville inconnue";
        }
        return name;
    }

    public String getStreetName(String latitude, String longitude) {
        prepareRequest(latitude,longitude);
        String name;
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofString());
            JSONObject json = new JSONObject(response.body());
            name = json.getJSONArray("results").getJSONObject(0).getJSONObject("components").getString("road");
        } catch (Exception e) {
            name = "Rue inconnue";
        }
        return name;
    }
}
