package com.irit.mapsender.model.tools;

import com.irit.mapsender.model.annotations.ParametrableComponent;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.beans.Introspector;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class ParametrableComponentsScanner {

    private List<String> packages;
    private List<Class> parametrableComponents;
    private Map<String,String> componentsNameMap;
    private ClassPathScanningCandidateComponentProvider scanner;

    public ParametrableComponentsScanner() {
        scanner = new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(ParametrableComponent.class));
        scanParametrableComponents();
        initComponentNameMap();
        loadSVGs();
    }

    private void initComponentNameMap() {
        componentsNameMap = new HashMap<>();
        for (Class clazz : parametrableComponents) {
            String key = Introspector.decapitalize(clazz.getSimpleName().split("Component")[0]);
            String value = clazz.getCanonicalName();
            componentsNameMap.put(key,value);
        }
    }

    private void scanParametrableComponents() {
        parametrableComponents = new ArrayList<Class>();
        if (packages == null) packages = Arrays.asList("cn", "com");
        for (var packageName : packages) {
            var beans = scanner.findCandidateComponents(packageName);
            for (var bean : beans) {
                try {
                    var className = bean.getBeanClassName();
                    Class clazz = Class.forName(className);

                    parametrableComponents.add(clazz);
                } catch (ClassNotFoundException e) {
                    System.out.println("not found class:" + bean.getBeanClassName() + e);
                }
            }
        }
    }

    private void loadSVGs() {
        for (Class clazz : parametrableComponents ) {
            if (!Files.exists(Path.of(ComponentDiagramDrawer.FILE_PATH + Introspector.decapitalize(clazz.getSimpleName().split("Component")[0]) + ComponentDiagramDrawer.EXTENSION))) {
                ParametrableComponent annotation = (ParametrableComponent) clazz.getAnnotation(ParametrableComponent.class);
                ComponentDiagramDrawer.drawComponent(annotation.requires(),annotation.provides(),Introspector.decapitalize(clazz.getSimpleName().split("Component")[0]));
            }
        }
    }

    public List<String> getComponentsNames() {
        return new ArrayList<>(componentsNameMap.keySet());
    }

    public Map<String,String> getComponentsNameMap() {
        return componentsNameMap;
    }

    public String getAllAnnotations() {
        JSONObject composants = new JSONObject();
        for (Class clazz : parametrableComponents) {
            JSONObject composant = new JSONObject();
            ParametrableComponent annotation = (ParametrableComponent) clazz.getAnnotation(ParametrableComponent.class);
            composant.put("requires",annotation.requires());
            composant.put("provides",annotation.provides());
            composant.put("description", annotation.description());
            composant.put("hasIHM",annotation.hasIHM());
            composant.put("url",annotation.url());
            composants.put(Introspector.decapitalize(clazz.getSimpleName().split("Component")[0]),composant);
        }

        return composants.toString();
    }

    public List<Class> getParametrableComponents() {
        return parametrableComponents;
    }
}
