package com.irit.mapsender.model.tools;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PropertiesReader {

    private final static String FILE_PATH = new File("").getAbsolutePath().concat("/src/main/resources/components.properties");
    private Properties prop;

    private ParametrableComponentsScanner scanner;
    private List<String> unusedProperties;

    public PropertiesReader(){
        scanner = new ParametrableComponentsScanner();
        try (InputStream input = new FileInputStream(FILE_PATH)) {

            prop = new Properties();
            prop.load(input);
            unusedProperties = getAllKeys();
            initPropertiesFile();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initPropertiesFile() {
        for (String name : scanner.getComponentsNames()) {
            if (unusedProperties.contains(name)){
                unusedProperties.remove(name);
            }
            String value = getProperty(name);
            editProperty(name,"0");
        }
        if (unusedProperties.size() > 0) {
            for (String name : unusedProperties) {
                removeProperty(name);
            }
        }
    }

    public void removeProperty(String name) {
        try (OutputStream output = new FileOutputStream(FILE_PATH)) {

            prop.remove(name);
            prop.store(output,null);

        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void editProperty(String name, String value){
        try (OutputStream output = new FileOutputStream(FILE_PATH)) {

            prop.setProperty(name,value);
            prop.store(output,null);

        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Pair<String,String>> getAllProperties() {
        ArrayList<Pair<String,String>> list = new ArrayList<>();
        for (Object key: prop.keySet()){
            list.add(new Pair<>(key.toString(), prop.getProperty(key.toString())));
        }
        return list;
    }

    public List<String> getAllKeys(){
        ArrayList<String> list = new ArrayList<>();
        for (Object key: prop.keySet()){
            list.add(key.toString());
        }
        return list;
    }

    public String getProperty(String key) {
        return prop.getProperty(key);
    }
}
