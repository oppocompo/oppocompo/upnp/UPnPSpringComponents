package com.irit.mapsender.model.exceptions;

public class NotADisplayableComponentException extends Exception{

    public NotADisplayableComponentException(String name) {
        super(name + " is not a displayable component");
    }
}
