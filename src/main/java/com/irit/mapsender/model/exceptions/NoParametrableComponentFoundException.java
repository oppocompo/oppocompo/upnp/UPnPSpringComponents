package com.irit.mapsender.model.exceptions;

public class NoParametrableComponentFoundException extends Exception {

    public NoParametrableComponentFoundException(String name){
        super("No parametrable component named " + name + ".\n"
        + "Maybe you forgot to annotate your new component with @ParametrableComponent ?");
    }
}
