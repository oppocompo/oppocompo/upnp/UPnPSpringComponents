package com.irit.mapsender.model.exceptions;

public class ErrorWhileCreatingComponentException extends Exception {

    public ErrorWhileCreatingComponentException(String className) {
        super(  "Error while creating "+ className + "component.\n"+
                "It means either the component itself or the services used inside are incorrects.");
    }
}
