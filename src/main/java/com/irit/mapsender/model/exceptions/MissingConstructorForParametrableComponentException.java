package com.irit.mapsender.model.exceptions;

public class MissingConstructorForParametrableComponentException extends Exception {

    public MissingConstructorForParametrableComponentException(String className) {
        super("Invalid constructor for Class " + className + ".\n" +
                "ParametrableComposant should have a constructor with a String argument 'name' used to choose"+
                " the name of the component.");
    }
}
