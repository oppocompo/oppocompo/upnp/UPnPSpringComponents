package com.irit.mapsender.model.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ParametrableComponent {
    String[] requires() default {};
    String[] provides() default {};
    String description() default "";
    boolean hasIHM() default false;
    String url() default "/";
}
